﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using System.IO;
using FolderWatcher.Core;

namespace FolderWatcher.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void FolderWatcherService_ShouldNotifyObserverAboutFileChanges()
        {
            var folder = @"F:\devel\tmp";
            var observer = new StubFolderChangeObserver();
            var service = new FolderWatcherService(folder, observer);

            service.Start();
            GC.Collect();

            Directory.SetCurrentDirectory(folder);

            string first = "1.txt", second = "2.txt", third = "3.txt";

            using (var writer = File.CreateText(first)) {
                writer.WriteLine("test");
            }
            File.Copy(first, second);
            File.Move(first, third);
            using (var writer = File.AppendText(second)) {
                writer.WriteLine("second test");
            }
            File.Delete(second);
            File.Delete(third);

            var expected = new List<Tuple<string, string>>
            {
                new Tuple<string, string>("Created", first),
                new Tuple<string, string>("Changed", first),
                new Tuple<string, string>("Created", second),
                new Tuple<string, string>("Changed", second),
                new Tuple<string, string>("Changed", second),
                new Tuple<string, string>("Renamed", first + " -> " + third),
                new Tuple<string, string>("Changed", second),
                new Tuple<string, string>("Deleted", second),
                new Tuple<string, string>("Deleted", third)
            };

            Assert.Equal(expected, observer.FolderStatusChanges);
        }
    }
}

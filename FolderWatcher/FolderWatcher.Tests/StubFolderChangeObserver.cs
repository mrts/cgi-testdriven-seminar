﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FolderWatcher.Core;

namespace FolderWatcher.Tests
{
    class StubFolderChangeObserver : IFolderChangeObserver
    {
        private List<Tuple<string, string>> _statusChanges = new List<Tuple<string, string>>();
        public IList<Tuple<string, string>> FolderStatusChanges { get { return _statusChanges; } }

        public void OnFolderStatusChanged(string activity, string filename)
        {
            _statusChanges.Add(new Tuple<string, string>(activity, filename));
        }
    }
}

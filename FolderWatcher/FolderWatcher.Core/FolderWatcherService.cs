﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace FolderWatcher.Core
{
    public class FolderWatcherService
    {
        private string folder;
        private IFolderChangeObserver observer;
        private FileSystemWatcher watcher = new FileSystemWatcher();

        public FolderWatcherService(string folder, IFolderChangeObserver observer)
        {
            this.folder = folder;
            this.observer = observer;
        }

        public void Start()
        {
            watcher.Path = this.folder;
            watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
                | NotifyFilters.FileName | NotifyFilters.DirectoryName;

            watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.Created += new FileSystemEventHandler(OnChanged);
            watcher.Deleted += new FileSystemEventHandler(OnChanged);
            watcher.Renamed += new RenamedEventHandler(OnRenamed);

            watcher.EnableRaisingEvents = true;

        }


        private void OnChanged(object source, FileSystemEventArgs args)
        {
            this.observer.OnFolderStatusChanged(args.ChangeType.ToString(), args.Name);
        }

        private void OnRenamed(object source, RenamedEventArgs args)
        {
            this.observer.OnFolderStatusChanged(args.ChangeType.ToString(),
                String.Format("{0} -> {1}", args.OldName, args.Name));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FolderWatcher.Core
{
    public interface IFolderChangeObserver
    {
        void OnFolderStatusChanged(string action, string filename);
    }
}
